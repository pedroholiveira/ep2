package gfx;

import java.awt.image.BufferedImage;

public class Assets {

    private static final int width = 10, height = 10;

    public static BufferedImage snake, body, kitty, bodyKitty, star, bodyStar, fruit, bigFruit, decreaseFruit, bombFruit, wall;

    public static void init() {
        SpriteSheet sheet = new SpriteSheet(ImageLoader.loadImage("/textures/snake.png"));
        SpriteSheet sheetBody = new SpriteSheet(ImageLoader.loadImage("/textures/body.png"));
        SpriteSheet sheet1 = new SpriteSheet(ImageLoader.loadImage("/textures/fruit.png"));
        SpriteSheet sheetKitty = new SpriteSheet(ImageLoader.loadImage("/textures/kitty.png"));
        SpriteSheet sheetBodyKitty = new SpriteSheet(ImageLoader.loadImage("/textures/bodyKitty.png"));
        SpriteSheet sheetStar = new SpriteSheet(ImageLoader.loadImage("/textures/starSnake.png"));
        SpriteSheet sheetBodyStar = new SpriteSheet(ImageLoader.loadImage("/textures/bodyStar.png"));
        SpriteSheet sheetBigFruit = new SpriteSheet(ImageLoader.loadImage("/textures/bigFruit.png"));
        SpriteSheet sheetDecreaseFruit = new SpriteSheet(ImageLoader.loadImage("/textures/decreaseFruit.png"));
        SpriteSheet sheetBombFruit = new SpriteSheet(ImageLoader.loadImage("/textures/bombFruit.png"));
        SpriteSheet sheetWall = new SpriteSheet(ImageLoader.loadImage("/textures/wall.png"));
        
        snake = sheet.crop(0, 0, width, height);
        body = sheetBody.crop(0, 0, width, height);
        fruit = sheet1.crop(0, 0, width, height);
        kitty = sheetKitty.crop(0, 0, width, height);
        bodyKitty = sheetBodyKitty.crop(0, 0, width, height);
        star = sheetStar.crop(0, 0, width, height);
        bodyStar = sheetBodyStar.crop(0, 0, width, height);
        bigFruit = sheetBigFruit.crop(0, 0, width, height);
        decreaseFruit = sheetDecreaseFruit.crop(0, 0, width, height);
        bombFruit = sheetBombFruit.crop(0, 0, width, height);
        wall = sheetWall.crop(0, 0, 40, 40);
    }

}
