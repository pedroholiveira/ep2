package pack;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Menu {

    private JFrame frame;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Menu menu = new Menu();
                    menu.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public Menu() {
        frame = new JFrame();
        frame.setTitle("Menu Principal");
        frame.setBounds(100, 100, 500, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JButton play = new JButton("PLAY");
        play.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                MenuSelection m = new MenuSelection();
                m.setVisible(true);                
           }
        });
        play.setBounds(150, 200, 150, 40);

        frame.getContentPane().add(play);

        JButton instrucoes = new JButton("INSTRUCTIONS");
        instrucoes.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

            }
        });
        instrucoes.setBounds(150, 250, 150, 40);
        frame.getContentPane().add(instrucoes);

        JButton ranking = new JButton("RANKING");
        ranking.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

            }
        });
        ranking.setBounds(150, 300, 150, 40);
        frame.getContentPane().add(ranking);

        frame.setLocationRelativeTo(null);

    }

}
