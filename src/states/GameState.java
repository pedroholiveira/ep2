package states;

import entities.BigFruit;
import entities.BombFruit;
import entities.ClassicSnake;
import static entities.Creature.pointsEarned;
import entities.DecreaseFruit;
import entities.Fruit;

import entities.Snake;
import entities.SnakeKitty;
import entities.SnakeStar;
import entities.Wall;

import java.awt.Graphics;
import pack.Game;

public class GameState extends State {

    private Snake snake;
    private Game game;
    private Fruit fruit;
    private BigFruit bigFruit;
    private DecreaseFruit decreaseFruit;
    private BombFruit bombFruit;
    private Wall wall;
    private Wall wall1;
    private Wall wall2;
    private Wall wall3;
    boolean star;
    static int end = 0;
    
    public GameState(Game game, int snakeType) {
        super();
        this.game = game;
        switch (snakeType) {

            case 1:
                snake = new ClassicSnake(game, 100, 100);
 
                break;
            case 2:
                snake = new SnakeStar(game, 100, 100);

                star=true;
                pointsEarned = 2;
                break;

            case 3:
                snake = new SnakeKitty(game, 100, 100);

                break;

        }
        fruit = new Fruit(game, 100, 100);
        bigFruit = new BigFruit(game, 100, 100);
        decreaseFruit = new DecreaseFruit(game, 100, 100);
        bombFruit = new BombFruit(game, 100, 100);
        wall = new Wall(game, 50, 100);
        wall1 = new Wall(game, 50, 140);
        wall2 = new Wall(game, 210, 100);
        wall3 = new Wall(game, 210, 140);
        snake.setPoints(-3);
    }

    public void checkFruit() {
        if (snake.X[0] == fruit.getX() && snake.Y[0] == fruit.getY()) {
            fruit.fruitGenerator();
            snake.setPoints(snake.getPoints() + pointsEarned);
            snake.setBodySize(snake.getBodySize() + 1);
            System.out.println("PONTOS" + snake.getPoints());
        }
    }
    public void checkBigFruit() {
        if (snake.X[0] == bigFruit.getX() && snake.Y[0] == bigFruit.getY()) {
            bigFruit.fruitGenerator();
            snake.setPoints(snake.getPoints() + pointsEarned + 1 );
            snake.setBodySize(snake.getBodySize());
            System.out.println("PONTOS" + snake.getPoints());
        }
    }

    public void checkDecreaseFruit() {
        if (snake.X[0] == decreaseFruit.getX() && snake.Y[0] == decreaseFruit.getY()) {
            decreaseFruit.fruitGenerator();
            snake.setBodySize(3);
        }
    }

    public void checkBombFruit() {
        if (snake.X[0] == bombFruit.getX() && snake.Y[0] == bombFruit.getY()) {
            bombFruit.fruitGenerator();
            end++;
            if (end == 2) {
                game.stop();
            }
        }
    }
    public void printPoints(){
        
    }

    @Override
    public void tick() {
        snake.tick();
        checkFruit();
        checkBigFruit();
        checkDecreaseFruit();
        checkBombFruit();
    }

    @Override
    public void render(Graphics g) {
        snake.render(g);
        fruit.render(g);
        bigFruit.render(g);
        decreaseFruit.render(g);
        bombFruit.render(g);
        wall.render(g);
        wall1.render(g);
        wall2.render(g);
        wall3.render(g);
    }

}
