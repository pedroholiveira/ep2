package entities;

import gfx.Assets;
import java.awt.Graphics;
import java.util.Random;
import pack.Game;

public class Wall extends Creature {

    private Game game;
    Random random = new Random();

    public Wall(Game game, int x, int y) {
        super(x, y, Creature.DEFAULT_CREATURE_WIDTH, Creature.DEFAULT_CREATURE_HEIGHT);
        this.game = game;
    }


    @Override

    public void tick() {

    }

    @Override
    public void render(Graphics g) {
        g.drawImage(Assets.wall, x, y, 40, 40, null);
    }

}
