package entities;

import static entities.Creature.X;
import static entities.Creature.Y;
import gfx.Assets;
import java.awt.Graphics;
import pack.Game;

public class ClassicSnake extends Snake {

    private Game game;
    int i;

    public ClassicSnake(Game game, int x, int y) {
        super(game, x, y);
        this.game = game;
    }

    @Override
    public void tick() {
        checkCollision();
        move();
    }

    @Override
    public void render(Graphics g) {

        for (i = 0; i < bodySize; i++) {
            if (i == 0) {
                g.drawImage(Assets.snake, X[i], Y[i], width, height, null);
            } else {
                g.drawImage(Assets.body, X[i], Y[i], width, height, null);
            }
        }
    }
}
