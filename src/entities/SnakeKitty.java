package entities;

import static entities.Creature.X;
import static entities.Creature.Y;
import gfx.Assets;
import java.awt.Graphics;
import pack.Game;

public class SnakeKitty extends Snake {

    private Game game;
    int i;

    public SnakeKitty(Game game, int x, int y) {
        super(game, x, y);
        this.game = game;
    }

    @Override
    public void checkCollision() {
        for (int i = bodySize; i > 0; i--) {
            if ((i > 4) && (X[0] == X[i]) && (Y[0] == Y[i])) {
                game.stop();
            }

        }

        if (Y[0] > map - 10) {
            game.stop();
        }

        if (Y[0] < 0) {
            game.stop();
        }

        if (X[0] > map - 10) {
            game.stop();
        }

        if (X[0] < 0) {
            game.stop();
        }
    }
    
    @Override
    public void tick() {
        move();
    }

    @Override
    public void render(Graphics g) {

        for (i = 0; i < bodySize; i++) {
            if (i == 0) {
                g.drawImage(Assets.kitty, X[i], Y[i], width, height, null);
            } else {
                g.drawImage(Assets.bodyKitty, X[i], Y[i], width, height, null);
            }
        }
    }
}
