package entities;

import gfx.Assets;
import java.awt.Graphics;
import java.util.Random;
import pack.Game;

public class BombFruit extends Creature {

    private Game game;
    Random random = new Random();

    public BombFruit(Game game, int x, int y) {
        super(x, y, Creature.DEFAULT_CREATURE_WIDTH, Creature.DEFAULT_CREATURE_HEIGHT);
        this.game = game;
    }

    public void fruitGenerator() {
        x = random.nextInt(29) * 10;
        y = random.nextInt(29) * 10;
        if(y>90 && y<190 && x>40 && x<100) {
            fruitGenerator();
        }
        if(y>90 && y<190 && x>200 && x<260) {
            fruitGenerator();
        }
    }

    @Override

    public void tick() {

    }

    @Override
    public void render(Graphics g) {
        g.drawImage(Assets.bombFruit, x, y, width, height, null);
    }

}
