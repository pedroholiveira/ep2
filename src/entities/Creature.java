package entities;

import entities.Entity;

public abstract class Creature extends Entity {

    public static final int X[] = new int[300];
    public static final int Y[] = new int[300];
    public static final int DEFAULT_POINTS = -1;
    public static final int DEFAULT_SPEED = 10;
    public static final int DEFAULT_CREATURE_WIDTH = 10, DEFAULT_CREATURE_HEIGHT = 10;
    public static final int DEFAULT_MAP = 300;
    public static final int DEFAULT_BODY_SIZE = 3;
    public static int pointsEarned = 1;

    static int points;
    protected int speed;
    protected int map;
    protected int bodySize;

    public Creature(int x, int y, int width, int height) {
        super(x, y, width, height);
        points = DEFAULT_POINTS;
        speed = DEFAULT_SPEED;
        map = DEFAULT_MAP;
        bodySize = DEFAULT_BODY_SIZE;
    }

    public static int getPointsEarned() {
        return pointsEarned;
    }

    public static void setPointsEarned(int pointsEarned) {
        Creature.pointsEarned = pointsEarned;
    }

    public int getMap() {
        return map;
    }

    public void setMap(int map) {
        this.map = map;
    }

    public int getBodySize() {
        return bodySize;
    }

    public void setBodySize(int bodySize) {
        this.bodySize = bodySize;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

}
