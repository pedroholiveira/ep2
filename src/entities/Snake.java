package entities;

import gfx.Assets;
import java.awt.Graphics;
import pack.Game;

public class Snake extends Creature {

    private Game game;
    int i;

    public Snake(Game game, int x, int y) {
        super(x, y, Creature.DEFAULT_CREATURE_WIDTH, Creature.DEFAULT_CREATURE_HEIGHT);
        this.game = game;
        this.X[0] = x;
        this.Y[0] = y;
    }

    @Override
    public void tick() {
        checkCollision();
        move();
    }

    public void checkCollision() {

        for (int i = bodySize; i > 0; i--) {
            if ((i > 4) && (X[0] == X[i]) && (Y[0] == Y[i])) {
                game.stop();
            }

        }

        if (Y[0] > map - 10) {
            game.stop();
        }

        if (Y[0] < 0) {
            game.stop();
        }

        if (X[0] > map - 10) {
            game.stop();
        }

        if (X[0] < 0) {
            game.stop();
        }
        if(Y[0]>90 && Y[0]<180 && X[0]>40 && X[0]<90) {       
            game.stop();
        }
        if(Y[0]>90 && Y[0]<180 && X[0]>200 && X[0]<250) {
            game.stop();
        }

    }

    public void move() {

        for (i = bodySize; i > 0; i--) {
            X[i] = X[(i - 1)];
            Y[i] = Y[(i - 1)];
        }

        if (game.getKeyManager().up) {
            Y[0] -= speed;
        }
        if (game.getKeyManager().down) {
            Y[0] += speed;
        }
        if (game.getKeyManager().left) {
            X[0] -= speed;
        }
        if (game.getKeyManager().right) {
            X[0] += speed;
        }
    }

    @Override
    public void render(Graphics g) {

    }
}
